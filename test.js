import http from 'k6/http';
import { sleep } from 'k6';

export default function () {
  http.get('https://ppwvmq8v4a.execute-api.us-east-1.amazonaws.com/test/pets');
  sleep(30);
}